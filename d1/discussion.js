db.course_bookings.insertMany([
	{"courseID": "C001", "studentID": "S004", "isCompleted": true},
	{"courseID": "C002", "studentID": "S001", "isCompleted": false},
	{"courseID": "C001", "studentID": "S003", "isCompleted": true},
	{"courseID": "C003", "studentID": "S002", "isCompleted": false},
	{"courseID": "C001", "studentID": "S002", "isCompleted": true},
	{"courseID": "C004", "studentID": "S003", "isCompleted": false},
	{"courseID": "C002", "studentID": "S004", "isCompleted": true},
	{"courseID": "C003", "studentID": "S007", "isCompleted": false},
	{"courseID": "C001", "studentID": "S005", "isCompleted": true},
	{"courseID": "C004", "studentID": "S008", "isCompleted": false},
	{"courseID": "C001", "studentID": "S013", "isCompleted": true}
]);

/* Aggregation in MongoDB
	This is the act or process of generating manipulated data and perform operations to created filtered results that helps in analyzing data.
	This helps in creating reports from analyzing the data provided in our documents.

	Aggregation Pipeline Syntax:
		db.collection.aggregate([
			{Stage1}
			{Stage2}
			{Stage3}
		]);

	Aggregation Pipelines
		Aggregation is done 2-3 steps typically. The first pipeline was with the use of $match.
		
		$match is used to pass the documents or get the documents that will match our condition

		Syntax:
			{$match: {field: value}}

		$group is used to group elements/documents together and create an analysis of these grouped documents.

		Syntax:
			{$group: {_id: <id>, fieldResult: "valueResult"}}
		
		$project allows us to show or hide details.

		Syntax:
			{$project: {field: 0 or 1}}

		$sort can be used to change the order of the aggregated results.

		Syntax:
			{$sort: {field: 1/-1}}

		$count - allows us to count the total number of items.

		Syntax: 
			{$count: "string"}

*/

// Count all the documents in our collection

db.course_bookings.aggregate([
	{$group: {_id: null, count: {$sum: 1}}}
]);

// find students that completed the course and group by courseID
db.course_bookings.aggregate([
	{$match: {"isCompleted": true}},//filter lahat ng docs na complete
	{$group: {_id: "$courseID", total: {$sum: 1}}}//iggroup
]);

//find students who completed the course and don't show the course and student ID
db.course_bookings.aggregate([
	{$match: {"isCompleted": true}},
	{$project: {"courseID": 0, "studentID": 0}}
]);

//find students who completed the course and sort by descending order
db.course_bookings.aggregate([
	{$match: {"isCompleted": true}},
	{$sort: {"courseID": -1}}
])

// count the completed courses of student s013
db.course_bookings.aggregate([
	{$match: {"studentID" : "S013", "isCompleted": true}},
	{$group: {_id: null, count: {$sum: 1}}}
])

/*db.course_bookings.aggregate([
    {$match: {studentId: "S013",isCompleted: true}},    
    {$count: "courseId"}
])*/

db.orders.insertMany([
        {
                "cust_Id": "A123",
                "amount": 500,
                "status": "A"
        },
        {
                "cust_Id": "A123",
                "amount": 250,
                "status": "A"
        },
        {
                "cust_Id": "B212",
                "amount": 200,
                "status": "A"
        },
        {
                "cust_Id": "B212",
                "amount": 200,
                "status": "D"
        }
])
//get the total amount
db.orders.aggregate([
	{$match: {status: "A"}},
	{$group: {_id: "$cust_Id", total: {$sum: "$amount"}}}
]);

/* 
	$sum operator will total the values
 	$avg operator will average the results
	$max operator will show you the highest value
	$min operator will show you the lowest value
*/

//get the average amount
db.orders.aggregate([
	{$match: {status: "A"}},
	{$group: {_id: "$cust_Id", average_amount: {$avg: "$amount"}}}
]);

//get the highest amount per cust_Id
db.orders.aggregate([
	{$group: {_id: "$cust_Id", highAmount: {$max: "$amount"}}}
]);

/* db.orders.aggregate([
        {$match: {  "status": { $in:["A","D"] }}},
        {$group: { _id: "$cust_Id", maxAmount: {$max: "$amount"} }}
 ]);*/